﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAula05
{
    class Program
    {
        static void Main(string[] args)
        {
            int dices;
            string temp;
            int sum = 0;
            Random rand = new Random();

            Console.WriteLine("Insert number of dices");
            temp = Console.ReadLine();
            dices = Convert.ToInt32(temp);

            for (int i = 0; i < dices; i++)
            {
                sum += rand.Next(1, 7);
            }

            Console.WriteLine($"A soma do(s) {dices} dados é {sum}");



        }
    }
}
